package com.example.planque.notegps;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.EditText;

import java.util.Calendar;

public class ModifierNote extends Activity {

    public String getTitre;
    public String getTexte;
    LocationManager lm;
    Location location;
    int idNote;
    double longitude;
    double latitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        Intent intent = getIntent();

        final EditText titre = (EditText) findViewById(R.id.editText);
        final EditText text = (EditText) findViewById(R.id.edit);

        //Récupération coordonnées
        lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        try {
            longitude = location.getLongitude();
            latitude = location.getLatitude();
        }
        catch (Exception e){
            longitude = 0.0;
            latitude = 0.0;
        }

        Bundle donnees = intent.getExtras();

        if(donnees != null)
        {
            getTitre = (String) donnees.get("titre");
            getTexte = (String) donnees.get("texte");
            idNote = (Integer) donnees.get("id");
            System.out.println(idNote);
            titre.setText(getTitre);
            text.setText(getTexte);
            System.out.println(getTitre);
            System.out.println(getTexte);
       }

        titre.setText(donnees.get("titre").toString());
        text.setText(donnees.get("texte").toString());
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if( titre.getText().toString().trim().equals("")){
                    titre.setError( "Titre obligatoire !" );
                }
                else {
                    modifierNote(idNote, titre.getText().toString(), text.getText().toString(), getDate(), latitude, longitude);
                }
            }
        });
    }

    public void modifierNote(int id, String titre, String texte, String date, double latitude, double longitude) {
        Intent previousMainActivity = new Intent(this, MainActivity.class);
        previousMainActivity.putExtra("idNote", id);
        previousMainActivity.putExtra("titre", titre);
        previousMainActivity.putExtra("texte", texte);
        previousMainActivity.putExtra("date", date);
        previousMainActivity.putExtra("latitude",latitude);
        previousMainActivity.putExtra("longitude", longitude);
        setResult(RESULT_OK, previousMainActivity);
        finish();
    }

    public String getDate() {
        Calendar rightNow = Calendar.getInstance();
        return rightNow.get(Calendar.DAY_OF_MONTH) + "/" + rightNow.get(Calendar.MONTH) + "/" + rightNow.get(Calendar.YEAR) + ":" + rightNow.get(Calendar.SECOND);
    }

}