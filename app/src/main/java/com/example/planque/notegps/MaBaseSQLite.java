package com.example.planque.notegps;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class MaBaseSQLite extends SQLiteOpenHelper {

    private static final String TABLE_NOTE = "table_notes";
    private static final String COL_ID = "ID";
    private static final String COL_TITRE = "TITRE";
    private static final String COL_TEXT = "TEXT";
    private static final String COL_DATE= "DATE";
    private static final String COL_LAT= "LATITUDE";
    private static final String COL_LON= "LONGITUDE";

    private static final String CREATE_BDD = "CREATE TABLE " + TABLE_NOTE + " ("
            + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_TITRE + " TEXT NOT NULL, "
            + COL_TEXT + " TEXT NOT NULL, "+ COL_DATE + " TEXT NOT NULL,"
            + COL_LAT+" TEXT NOT NULL, "+COL_LON+" TEXT NOT NULL);";

    public MaBaseSQLite(Context context, String name, CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //on créé la table à partir de la requête écrite dans la variable CREATE_BDD
        db.execSQL(CREATE_BDD);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //db.execSQL("DROP TABLE " + TABLE_NOTE + ";");
        //onCreate(db);
    }
}
